# Predictive Analysis
This is a collection of work representing progress on the predictive analysis project in ST491 at UMT.

## Data
I have uploaded the sample data that the programs use [here](https://bitbucket.org/benjic/st491/downloads/QuotesLong.txt). Your pull will not get the data needed by the programs.

Please direct your queries to [me](mailto:benjamin.campbell@mso.umt.edu).
